package dev.challenge.vindinium.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import dev.challenge.vindinium.GamesManagerActor;

@Configuration
@EnableAutoConfiguration
public class RootApplicationConfig {

    @Bean
    public ActorSystem actorSystem() throws Exception {
        Config sysConfig = ConfigFactory.load("application.akka.conf");
        ActorSystem system = ActorSystem.create("vindiniumBotSystem", sysConfig);

        // mock initial state generator
//        system.actorOf(MockInitialStateGenerator.props(), "mockVindinium-InitialState-server");

//        Camel camel = CamelExtension.get(system);
//        CamelContext camelContext = camel.context();
//        camelContext.addComponent("jetty", new EmbeddedJetty());
//        WebsocketComponent websocketComponent = camelContext.getComponent("websockets", WebsocketComponent.class);
//        websocketComponent.setStaticResources("classpath:BOOT-INF/classes/static");
//        camelContext.addComponent("activemq",
//                activeMQComponent("tcp://brk01.games-platform-qa.dev.gamesys.corp:61616"));

        Thread.sleep(500L);

        ActorRef gameManagerActor = system.actorOf(Props.create(GamesManagerActor.class), "gamesManagerActor");


//        ActorRef publisher = system.actorOf(Publisher.props(serverIp, serverPort, websocketPath), "publisher");
//        ActorRef hostApp = system.actorOf(HostApp.props(Long.valueOf(hostAppStatePublishingFrequency), actorFactory(), processorConfigMap(), publisher), "hostapp");
//        system.actorOf(CasinoEventReader.props(hostApp), "reader");

        Thread.sleep(500L);


        return system;
    }

}
